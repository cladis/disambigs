package disambigs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import org.wikipedia.BaseBot;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class Disambigs {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws javax.security.auth.login.FailedLoginException
     */
    public static void main(String[] args)
            throws IOException, FailedLoginException, LoginException {
        BaseBot w = new BaseBot("uk.wikipedia.org");
        w.login(args[0], args[1]);
        Map<String, BaseBot.BaseBotProppedPage> proppedPages = w.allPagesDisambigs();
        Set<String> keySet = proppedPages.keySet();
        Map<String, Set<String>> nameNumber = new HashMap<>();
        Map<String, Set<String>> nameNumberFilteredUnsorted = new HashMap<>();
        TreeMap<String, Set<String>> nameNumberFiltered = new TreeMap<>();

        for (String page : keySet) {
            String name = ambiguate(page);
            if (nameNumber.containsKey(name)) {
                Set<String> pagesWithName = nameNumber.get(name);
                pagesWithName.add(page);
                nameNumber.put(name, pagesWithName);
            } else {
                Set<String> pagesWithName = new HashSet<>();
                pagesWithName.add(page);
                nameNumber.put(name, pagesWithName);
            }
        }

        Set<String> names = nameNumber.keySet();
        namesIterator:
        for (String name : names) {
            Set<String> pagesWithName = nameNumber.get(name);
            if (pagesWithName.size() <= 1) {
                continue namesIterator;
            }

            boolean hasDisambig = false;

            HashSet<String> pagesWithNameToRemove = new HashSet<>();
            pagesWithNameIterator:
            for (String pageWithName : pagesWithName) {
                BaseBot.BaseBotProppedPage proppedPage = proppedPages.get(pageWithName);
                boolean isDisambig = proppedPage.isDisambig;
                boolean isRedirect = proppedPage.isRedirect;
                if (isRedirect) {
                    String redirectTarget = ambiguate(proppedPage.redirectTarget);
                    if (pagesWithName.contains(redirectTarget)) {
                        pagesWithNameToRemove.add(pageWithName);
                        System.out.println("We are about to remove\t" + pageWithName);
                        continue pagesWithNameIterator;
                    } else {
                        System.out.println("We are not going to remove\t" + pageWithName + "\t target:\t" + redirectTarget);
                    }
                } else {
                    System.out.println("Is not redirect:\t" + pageWithName);

                }

                if (isDisambig) {
                    hasDisambig = true;
                    break pagesWithNameIterator;
                } else if (isRedirect) {
                    String redirectTarget = proppedPage.redirectTarget;
                    BaseBot.BaseBotProppedPage proppedTarget = proppedPages.get(redirectTarget);
                    boolean isTargetDisambig = proppedTarget.isDisambig;
                    if (isTargetDisambig) {
                        hasDisambig = true;
                        break pagesWithNameIterator;
                    }
                }
            }
            pagesWithName.removeAll(pagesWithNameToRemove);

            if (hasDisambig) {
                continue namesIterator;
            } else {
                if (pagesWithName.size() > 1) {
                    nameNumberFilteredUnsorted.put(name, pagesWithName);
                }
            }
        }

        ArrayList<String> keys = new ArrayList<>();
        keys.addAll(nameNumberFilteredUnsorted.keySet());
        Collections.sort(keys);
        for (String key : keys) {
            nameNumberFiltered.put(key, nameNumberFilteredUnsorted.get(key));
        }

        String header = "Lists pages with the same name before (clarification), "
                + "but without a related disambiguation page.\n"
                + "{| class=\"wikitable sortable\"\n! name !! pages\n";
        String wrt = "";
        String pairsWrt = "";
        String footer = "|}";

        int iterator = 0;
        int pairsIterator = 0;
        int blanked = 0;
        int pairsBlanked = 0;
        Set<String> namesFilted = nameNumberFiltered.navigableKeySet();

        /*
        * TODO: move to some config or whatever
         */
        String disambiguation;
        switch (w.getDomain()) {
            case "be-tarask.wikipedia.org":
                disambiguation = "неадназначнасьць";
                break;
            case "de.wikipedia.org":
                disambiguation = "Begriffsklärung";
                break;
            case "ru.wikipedia.org":
                disambiguation = "значения";
                break;
            case "uk.wikipedia.org":
                disambiguation = "значення";
                break;
            default:
                disambiguation = "disambiguation";
        }

        for (String nameFilted : namesFilted) {
            Set<String> pagesWithNameFiltered = nameNumberFiltered.get(nameFilted);
            boolean moreThanTwo = pagesWithNameFiltered.size() > 2;
            if (moreThanTwo) {
                iterator++;
                wrt += "|-\n| [[" + (proppedPages.containsKey(nameFilted)
                        ? nameFilted + " (" + disambiguation + ")|" + nameFilted
                        : nameFilted) + "]]\n|\n";
            } else {
                pairsIterator++;
                pairsWrt += "|-\n| [[" + (proppedPages.containsKey(nameFilted)
                        ? nameFilted + " (" + disambiguation + ")|" + nameFilted
                        : nameFilted) + "]]\n|\n";
            }
            for (String pageWithNameFiltered : pagesWithNameFiltered) {
                if (moreThanTwo) {
                    wrt += "# [[" + pageWithNameFiltered + "]]\n";
                } else {
                    pairsWrt += "# [[" + pageWithNameFiltered + "]]\n";
                }
            }
            System.out.println(nameFilted + "\t" + pagesWithNameFiltered.size());
            if (iterator == 1000) {
                w.edit("user:BaseBot/DisambigsSuggestion/" + blanked,
                        header + wrt + footer,
                        "creation/updation");
                iterator = 0;
                wrt = "";
                blanked++;
            }
            if (pairsIterator == 1000) {
                w.edit("user:BaseBot/DisambigsSuggestion/pairs/" + pairsBlanked,
                        header + pairsWrt + footer,
                        "creation/updation");
                pairsIterator = 0;
                pairsWrt = "";
                pairsBlanked++;
            }
        }

        System.out.println(wrt);
        w.edit("user:BaseBot/DisambigsSuggestion/" + blanked,
                header + wrt + footer,
                "creation/updation");

        System.out.println(pairsWrt);
//        w.edit("user:BaseBot/DisambigsSuggestion/pairs/" + pairsBlanked,
//                header + pairsWrt + footer,
//                "creation/updation");

        String myDisambig = "";
        for (int i = 0; i <= blanked; i++) {
            myDisambig += "* [[/" + i + "]]\n";
        }
        for (int i = 0; i <= pairsBlanked; i++) {
            myDisambig += "* [[/pairs/" + i + "]]\n";
        }
        w.edit("user:BaseBot/DisambigsSuggestion", myDisambig, "creation/updating");
    }//main method

    /**
     * Removes "(something)" if it was there
     *
     * @param untruncatedName
     * @return
     */
    public static String ambiguate(String untruncatedName) {
        return (untruncatedName.contains("(")
                ? untruncatedName.substring(0, untruncatedName.indexOf("("))
                : untruncatedName).trim();
    }
}//class
