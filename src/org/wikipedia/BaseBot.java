/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wikipedia;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
//import static net.wikipedia.OldWiki.ALL_NAMESPACES;
import static org.wikipedia.Wiki.UPLOAD_LOG;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class BaseBot extends WMFWiki {

    /**
     * Creates a new wiki that has the given domain name.
     *
     * @param domain a wiki domain name e.g. en.wikipedia.org
     */
    public BaseBot(String domain) {
        super(domain);
        this.setMarkBot(true);
        this.setMarkMinor(true);
        this.setUserAgent("BaseBot");
        this.setMaxLag(1000);
    }

    /**
     * @param title - title of the page in the site
     * @param site - the site the page is in
     * @return
     * @throws IOException
     */
    public boolean hasWDEntity(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "setProperty");
        boolean r = false;
        if (!line.contains("missing=\"\"")) {
            r = true;
        }
        return r;
    }

    /**
     * Checks if a user has uploads between the specified times.
     *
     * @param user the user to get uploads for
     * @param start the date to start enumeration (use null to not specify one)
     * @param end the date to end enumeration (use null to not specify one)
     * @return a list of all live images the user has uploaded
     * @throws IOException if a network error occurs
     */
    public boolean hasUploads(User user, Calendar start, Calendar end) throws IOException {
        StringBuilder url = new StringBuilder(query);
        url.append("list=allimages&ailimit=1&aisort=timestamp&aiprop=timestamp%7Ccomment&aiuser=");
        url.append(encode(user.getUsername(), false));
        if (start != null) {
            url.append("&aistart=");
            url.append(calendarToTimestamp(start));
        }
        if (end != null) {
            url.append("&aiend=");
            url.append(calendarToTimestamp(end));
        }
        List<LogEntry> uploads = new ArrayList<>();

        String line = fetch(url.toString(), "getUploads");

        Boolean result = line.contains("<img ");

        log(Level.INFO, "getUploads", "Successfully retrieved uploads of\t" + user.getUsername() + "\t(\t" + result + "\t)");

        return result;
    }

    public Map<String, BaseBotProppedPage> allPagesDisambigs() throws IOException {
//w/api.php?action=query&format=xml&prop=pageprops&list=&generator=allpages&ppcontinue=&ppprop=disambiguation&gapcontinue=&gaplimit=10     
        boolean isBot = this.user.isAllowedTo("bot");
        StringBuilder url = new StringBuilder(query);
        url.append("prop=pageprops|redirects"
                + "&generator=allpages"
                + "&ppcontinue="
                + "&ppprop=disambiguation"
                + "&gaplimit=" + (isBot ? 5000 : 500) 
                + "&rdlimit=" + (isBot ? 3000 : 300) 
                + "&rdnamespace=0"
                + "&gapnamespace=0"
                + "&gapcontinue=");
        Map<String, BaseBotProppedPage> pages = new HashMap<>();
        String urlString = url.toString();
        String gapcontinue = "";

        do {
            String line = fetch(urlString + gapcontinue, "allPagesDisambigs");
            System.out.println(line);
            if (line.contains("gapcontinue")) {
                gapcontinue = parseAttribute(line, "gapcontinue", 0);
            } else {
                gapcontinue = "";
            }
            String[] pageSplit = line.split("<page ");
            for (int i = 1; i < pageSplit.length; i++) {
                String page = pageSplit[i];
                String title = parseAttribute(page, "title", 0);
                boolean isDisambig = page.contains("disambiguation=\"\"");
                if (pages.containsKey(title)) {
                    BaseBotProppedPage proppedPage = pages.get(title);
                    proppedPage.isDisambig = isDisambig;
                    pages.put(title, proppedPage);
                } else {
                    BaseBotProppedPage proppedPage = new BaseBotProppedPage(title, isDisambig);
                    pages.put(title, proppedPage);
                }
                if (page.contains("<redirects>")) {
                    boolean isRedirect = true;
                    String[] redirectSplit = line.split("<rd ");
                    for (int j = 1; j < redirectSplit.length; j++) {
                        String redirect = redirectSplit[j];
                        String redirectSource = parseAttribute(redirect, "title", 0);
                        if (pages.containsKey(redirectSource)) {
                            BaseBotProppedPage proppedPage = pages.get(redirectSource);
                            proppedPage.isRedirect = isRedirect;
                            proppedPage.redirectTarget = title;
                            pages.put(redirectSource, proppedPage);
                        } else {
                            BaseBotProppedPage proppedPage = new BaseBotProppedPage(redirectSource, isRedirect, title);
                            pages.put(redirectSource, proppedPage);
                        }
                    }
                }
            }
        } while (!gapcontinue.equals(""));

        return pages;
    }

    /**
     *
     * @param name
     * @param depth
     * @param ns
     * @return
     * @throws IOException
     */
    public String[] getCategoryMembers(String name, short depth, int... ns) throws IOException {
        return getCategoryMembers(name, depth, new ArrayList<String>(), ns);
    }

    public String[] getUnreviewedPages(String urfilterredir, int... ns) throws IOException {
        Set<String> pages = new HashSet<>();
        StringBuilder url = new StringBuilder(query + "list=unreviewedpages");
        url.append("&urfilterredir=" + urfilterredir);
        url.append("&urfilterlevel=" + 0);
        url.append("&urlimit=max");
        String namespaces = "";
        for (int namespace : ns) {
            namespaces += namespaces.equals("") ? namespace : "|" + namespace;
        }
        url.append("&urnamespace=" + namespaces);
        String urstart = "";

        String urlString = url.toString();
        do {
            String line = fetch(
                    urlString + (urstart.equals("") ? "" : "&urstart=") + urstart,
                    "getUnreviewedPages"
            );
            if (line.contains("<query-continue>")) {
                urstart = parseAttribute(line, "urstart", 0);
            } else {
                urstart = "";
            }

            if (line.contains("<unreviewedpages>")) {
                String[] split = line.split("<p ");
                for (int i = 1; i < split.length; i++) {
                    String string = split[i];
                    String page = parseAttribute(string, "title", 0);
                    pages.add(page);
                }
            }
        } while (!urstart.equals(""));
        int size = pages.size();
        log(
                Level.INFO,
                "getCategoryMembers",
                "Successfully retrieved list of unreviewed pages for namespace(s) " + namespaces + " (" + size + " items)"
        );
        return pages.toArray(new String[size]);
    }

    public class BaseBotProppedPage {

        public String page;
        public boolean isDisambig;
        public boolean isRedirect = false;
        public String redirectTarget;

        BaseBotProppedPage(String page, boolean isDisambig) {
            this.page = page;
            this.isDisambig = isDisambig;
        }

        BaseBotProppedPage(String page, boolean isRedirect, String redirectTarget) {
            this.page = page;
            this.isRedirect = isRedirect;
            this.redirectTarget = redirectTarget;
        }
    }

    public String[] allPages(String title, int namespace, boolean redirects) throws IOException {
        StringBuilder url = new StringBuilder(query);
        url.append("action=query&list=allpages&aplimit=max&apfrom=");
        url.append(URLEncoder.encode(title, "UTF-8"));
        if (namespace != ALL_NAMESPACES) {
            url.append("&apnamespace=");
            url.append(namespace);
        }
        if (redirects) {
            url.append("&apfilterredir=redirects");
        }

        // main loop
        ArrayList<String> pages = new ArrayList<String>(6667); // generally enough
        String temp = url.toString();
        String next = "";
        do {
            // fetch data
            String line = fetch(temp + next, "allPages");

            // set next starting point
            if (line.contains("apcontinue")) {
                int a = line.indexOf("apcontinue=\"") + 12;
                int b = line.indexOf('\"', a);
                next = "&apcontinue=" + line.substring(a, b);
            } else {
                next = "done";
            }

            // parse items
            while (line.contains("title")) {
                int x = line.indexOf("title=\"");
                int y = line.indexOf("\" ", x);
                pages.add(decode(line.substring(x + 7, y)));
                line = line.substring(y + 4);
            }
        } while (!next.equals("done"));

        log(Level.INFO, "Successfully retrieved " + (redirects ? "list of pages " : "list of redirects") + " (" + pages.size() + " items)", "allPages");
        return pages.toArray(new String[0]);
    }

}
